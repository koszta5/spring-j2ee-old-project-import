package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PCirkve;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pcirkves", formBackingObject = PCirkve.class)
@RequestMapping("/pcirkves")
@Controller
public class PCirkveController {
}
