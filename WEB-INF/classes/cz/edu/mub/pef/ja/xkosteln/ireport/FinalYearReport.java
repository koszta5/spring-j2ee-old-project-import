package cz.edu.mub.pef.ja.xkosteln.ireport;

import java.sql.SQLException;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;



public class FinalYearReport {




private String pathToJRXML;



public String getPathToJRXML() {
	return pathToJRXML;
}

public void setPathToJRXML(String pathToJRXML) {
	this.pathToJRXML = pathToJRXML;
}

public void createReport(String pathToJRXML){
this.setPathToJRXML(pathToJRXML);
try {
	JasperDesign jd=JRXmlLoader.load(pathToJRXML);
	JasperReport jr=JasperCompileManager.compileReport(jd);
	DataSourceProvider ds=new DataSourceProvider();
	HashMap<String, String> map = new HashMap<String, String>();
    map.put("REPORTNAME", "Finalni report");
	JasperPrint jp=JasperFillManager.fillReport(jr,map,ds.getDs().getConnection());
	String user_home=System.getProperty("user.home");
    JasperExportManager.exportReportToPdfFile(jp, user_home+"/Vyrocni_report.pdf");
} catch (JRException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
catch (SQLException e){
	e.printStackTrace();
}

}


}
