package cz.edu.mub.pef.ja.xkosteln.ireport;

import org.apache.commons.dbcp.BasicDataSource;

public class DataSourceProvider {
private BasicDataSource ds;
public DataSourceProvider(){
	this.ds=new BasicDataSource();
	String url="jdbc:mysql://blabla/blabla?user=blabla&password=blabla";
	ds.setUrl(url);
	
}
public BasicDataSource getDs() {
	return ds;
}
public void setDs(BasicDataSource ds) {
	this.ds = ds;
}

}
