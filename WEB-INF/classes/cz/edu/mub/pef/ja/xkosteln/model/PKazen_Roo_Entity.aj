// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package cz.edu.mub.pef.ja.xkosteln.model;

import cz.edu.mub.pef.ja.xkosteln.model.PKazen;
import java.lang.Integer;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PKazen_Roo_Entity {
    
    declare @type: PKazen: @Entity;
    
    declare @type: PKazen: @Table(name = "p_kazen", catalog = "kosta");
    
    @PersistenceContext
    transient EntityManager PKazen.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_kazen")
    private Integer PKazen.idKazen;
    
    public Integer PKazen.getIdKazen() {
        return this.idKazen;
    }
    
    public void PKazen.setIdKazen(Integer id) {
        this.idKazen = id;
    }
    
    @Transactional
    public void PKazen.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void PKazen.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            PKazen attached = this.entityManager.find(this.getClass(), this.idKazen);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void PKazen.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public PKazen PKazen.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        PKazen merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager PKazen.entityManager() {
        EntityManager em = new PKazen().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long PKazen.countPKazens() {
        return entityManager().createQuery("select count(o) from PKazen o", Long.class).getSingleResult();
    }
    
    public static List<PKazen> PKazen.findAllPKazens() {
        return entityManager().createQuery("select o from PKazen o", PKazen.class).getResultList();
    }
    
    public static PKazen PKazen.findPKazen(Integer id) {
        if (id == null) return null;
        return entityManager().find(PKazen.class, id);
    }
    
    public static List<PKazen> PKazen.findPKazenEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("select o from PKazen o", PKazen.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
