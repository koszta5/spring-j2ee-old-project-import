// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package cz.edu.mub.pef.ja.xkosteln.model;

import cz.edu.mub.pef.ja.xkosteln.model.PUkonceniClenstvi;
import java.lang.Integer;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PUkonceniClenstvi_Roo_Entity {
    
    declare @type: PUkonceniClenstvi: @Entity;
    
    declare @type: PUkonceniClenstvi: @Table(name = "p_ukonceni_clenstvi", catalog = "kosta");
    
    @PersistenceContext
    transient EntityManager PUkonceniClenstvi.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_ukonceni")
    private Integer PUkonceniClenstvi.idUkonceni;
    
    public Integer PUkonceniClenstvi.getIdUkonceni() {
        return this.idUkonceni;
    }
    
    public void PUkonceniClenstvi.setIdUkonceni(Integer id) {
        this.idUkonceni = id;
    }
    
    @Transactional
    public void PUkonceniClenstvi.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void PUkonceniClenstvi.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            PUkonceniClenstvi attached = this.entityManager.find(this.getClass(), this.idUkonceni);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void PUkonceniClenstvi.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public PUkonceniClenstvi PUkonceniClenstvi.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        PUkonceniClenstvi merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager PUkonceniClenstvi.entityManager() {
        EntityManager em = new PUkonceniClenstvi().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long PUkonceniClenstvi.countPUkonceniClenstvis() {
        return entityManager().createQuery("select count(o) from PUkonceniClenstvi o", Long.class).getSingleResult();
    }
    
    public static List<PUkonceniClenstvi> PUkonceniClenstvi.findAllPUkonceniClenstvis() {
        return entityManager().createQuery("select o from PUkonceniClenstvi o", PUkonceniClenstvi.class).getResultList();
    }
    
    public static PUkonceniClenstvi PUkonceniClenstvi.findPUkonceniClenstvi(Integer id) {
        if (id == null) return null;
        return entityManager().find(PUkonceniClenstvi.class, id);
    }
    
    public static List<PUkonceniClenstvi> PUkonceniClenstvi.findPUkonceniClenstviEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("select o from PUkonceniClenstvi o", PUkonceniClenstvi.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
