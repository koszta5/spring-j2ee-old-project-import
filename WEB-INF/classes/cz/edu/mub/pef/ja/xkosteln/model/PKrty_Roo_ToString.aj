// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package cz.edu.mub.pef.ja.xkosteln.model;

import java.lang.String;

privileged aspect PKrty_Roo_ToString {
    
    public String PKrty.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("POsoby: ").append(getPOsoby()).append(", ");
        sb.append("DatumKrtu: ").append(getDatumKrtu()).append(", ");
        sb.append("JmenoKrticiho: ").append(getJmenoKrticiho()).append(", ");
        sb.append("PrijmeniKrticiho: ").append(getPrijmeniKrticiho()).append(", ");
        sb.append("ObecKrtu: ").append(getObecKrtu()).append(", ");
        sb.append("IdKrtu: ").append(getIdKrtu());
        return sb.toString();
    }
    
}
