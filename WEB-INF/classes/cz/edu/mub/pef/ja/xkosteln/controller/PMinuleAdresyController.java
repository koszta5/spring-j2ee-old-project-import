package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PMinuleAdresy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pminuleadresys", formBackingObject = PMinuleAdresy.class)
@RequestMapping("/pminuleadresys")
@Controller
public class PMinuleAdresyController {
}
