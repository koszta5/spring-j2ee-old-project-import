package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PTypyKontaktu;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "ptypykontaktus", formBackingObject = PTypyKontaktu.class)
@RequestMapping("/ptypykontaktus")
@Controller
public class PTypyKontaktuController {
}
