package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PKrty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pkrtys", formBackingObject = PKrty.class)
@RequestMapping("/pkrtys")
@Controller
public class PKrtyController {
}
