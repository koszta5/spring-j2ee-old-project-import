package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PPscMesta;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "ppscmestas", formBackingObject = PPscMesta.class)
@RequestMapping("/ppscmestas")
@Controller
public class PPscMestaController {
}
