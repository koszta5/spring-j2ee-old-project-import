package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PDuvodyUkonceni;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pduvodyukoncenis", formBackingObject = PDuvodyUkonceni.class)
@RequestMapping("/pduvodyukoncenis")
@Controller
public class PDuvodyUkonceniController {
}
