package cz.edu.mub.pef.ja.xkosteln.model;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.dbre.RooDbManaged;

@RooJavaBean
@RooToString
@RooEntity(versionField = "", table = "p_psc_mesta", catalog = "kosta")
@RooDbManaged(automaticallyDelete = true)
public class PPscMesta {
}
