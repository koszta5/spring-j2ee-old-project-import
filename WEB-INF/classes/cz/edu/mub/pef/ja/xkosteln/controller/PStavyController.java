package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PStavy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pstavys", formBackingObject = PStavy.class)
@RequestMapping("/pstavys")
@Controller
public class PStavyController {
}
