package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PUkonceniClenstvi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pukonceniclenstvis", formBackingObject = PUkonceniClenstvi.class)
@RequestMapping("/pukonceniclenstvis")
@Controller
public class PUkonceniClenstviController {
}
