package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PKontakty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pkontaktys", formBackingObject = PKontakty.class)
@RequestMapping("/pkontaktys")
@Controller
public class PKontaktyController {
}
