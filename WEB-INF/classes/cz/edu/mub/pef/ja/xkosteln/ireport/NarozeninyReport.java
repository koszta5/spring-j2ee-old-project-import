package cz.edu.mub.pef.ja.xkosteln.ireport;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.dbcp.BasicDataSource;



public class NarozeninyReport {
private String pathToJRXML;

public NarozeninyReport(){

}
private BasicDataSource injectedBeanBasicDataSource;

public BasicDataSource getInjectedBeanBasicDataSource() {
	return injectedBeanBasicDataSource;
}

public void setInjectedBeanBasicDataSource(BasicDataSource injectedBeanBasicDataSource) {
	this.injectedBeanBasicDataSource = injectedBeanBasicDataSource;
	System.out.println("setting bean");
	
}

public String getPathToJRXML() {
	return pathToJRXML;
}

public void setPathToJRXML(String pathToJRXML) {
	this.pathToJRXML = pathToJRXML;
}

public void createReport(String path){
	this.setPathToJRXML(path);
/*	try {
		this.getInjectedBeanBasicDataSource().getConnection();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	try {
		JasperDesign JD=JRXmlLoader.load(new File(pathToJRXML));
		JasperReport JR=JasperCompileManager.compileReport(JD);
		
			//totally deprecated
			/*BasicDataSource mySource=new BasicDataSource();
			mySource.setUrl(url);*/
			DataSourceProvider ds=new DataSourceProvider();
			
			HashMap<String, String> map = new HashMap<String, String>();
		    map.put("REPORTNAME", "Narozeninovy report");
		    	
			  JasperPrint jasperPrint;
			try {
				jasperPrint = JasperFillManager.fillReport(JR, map,ds.getDs().getConnection());
				String user_home=System.getProperty("user.home");
			    JasperExportManager.exportReportToPdfFile(jasperPrint, user_home+"/narozeniny.pdf");			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  	
		
	  


	} catch (JRException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
}







	
	
}
