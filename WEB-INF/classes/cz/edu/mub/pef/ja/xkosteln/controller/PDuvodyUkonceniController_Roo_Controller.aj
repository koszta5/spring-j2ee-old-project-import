// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package cz.edu.mub.pef.ja.xkosteln.controller;

import cz.edu.mub.pef.ja.xkosteln.model.PDuvodyUkonceni;
import cz.edu.mub.pef.ja.xkosteln.model.PUkonceniClenstvi;
import java.io.UnsupportedEncodingException;
import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect PDuvodyUkonceniController_Roo_Controller {
    
    @Autowired
    private GenericConversionService PDuvodyUkonceniController.conversionService;
    
    @RequestMapping(method = RequestMethod.POST)
    public String PDuvodyUkonceniController.create(@Valid PDuvodyUkonceni pDuvodyUkonceni, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("pDuvodyUkonceni", pDuvodyUkonceni);
            return "pduvodyukoncenis/create";
        }
        pDuvodyUkonceni.persist();
        return "redirect:/pduvodyukoncenis/" + encodeUrlPathSegment(pDuvodyUkonceni.getIdDuvody().toString(), request);
    }
    
    @RequestMapping(params = "form", method = RequestMethod.GET)
    public String PDuvodyUkonceniController.createForm(Model model) {
        model.addAttribute("pDuvodyUkonceni", new PDuvodyUkonceni());
        return "pduvodyukoncenis/create";
    }
    
    @RequestMapping(value = "/{idDuvody}", method = RequestMethod.GET)
    public String PDuvodyUkonceniController.show(@PathVariable("idDuvody") Integer idDuvody, Model model) {
        model.addAttribute("pduvodyukonceni", PDuvodyUkonceni.findPDuvodyUkonceni(idDuvody));
        model.addAttribute("itemId", idDuvody);
        return "pduvodyukoncenis/show";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String PDuvodyUkonceniController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            model.addAttribute("pduvodyukoncenis", PDuvodyUkonceni.findPDuvodyUkonceniEntries(page == null ? 0 : (page.intValue() - 1) * sizeNo, sizeNo));
            float nrOfPages = (float) PDuvodyUkonceni.countPDuvodyUkoncenis() / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            model.addAttribute("pduvodyukoncenis", PDuvodyUkonceni.findAllPDuvodyUkoncenis());
        }
        return "pduvodyukoncenis/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public String PDuvodyUkonceniController.update(@Valid PDuvodyUkonceni pDuvodyUkonceni, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("pDuvodyUkonceni", pDuvodyUkonceni);
            return "pduvodyukoncenis/update";
        }
        pDuvodyUkonceni.merge();
        return "redirect:/pduvodyukoncenis/" + encodeUrlPathSegment(pDuvodyUkonceni.getIdDuvody().toString(), request);
    }
    
    @RequestMapping(value = "/{idDuvody}", params = "form", method = RequestMethod.GET)
    public String PDuvodyUkonceniController.updateForm(@PathVariable("idDuvody") Integer idDuvody, Model model) {
        model.addAttribute("pDuvodyUkonceni", PDuvodyUkonceni.findPDuvodyUkonceni(idDuvody));
        return "pduvodyukoncenis/update";
    }
    
    @RequestMapping(value = "/{idDuvody}", method = RequestMethod.DELETE)
    public String PDuvodyUkonceniController.delete(@PathVariable("idDuvody") Integer idDuvody, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        PDuvodyUkonceni.findPDuvodyUkonceni(idDuvody).remove();
        model.addAttribute("page", (page == null) ? "1" : page.toString());
        model.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/pduvodyukoncenis?page=" + ((page == null) ? "1" : page.toString()) + "&size=" + ((size == null) ? "10" : size.toString());
    }
    
    @ModelAttribute("pukonceniclenstvis")
    public Collection<PUkonceniClenstvi> PDuvodyUkonceniController.populatePUkonceniClenstvis() {
        return PUkonceniClenstvi.findAllPUkonceniClenstvis();
    }
    
    Converter<PDuvodyUkonceni, String> PDuvodyUkonceniController.getPDuvodyUkonceniConverter() {
        return new Converter<PDuvodyUkonceni, String>() {
            public String convert(PDuvodyUkonceni PDuvodyUkonceni) {
                return new StringBuilder().append(PDuvodyUkonceni.getNazev()).toString();
            }
        };
    }
    
    Converter<PUkonceniClenstvi, String> PDuvodyUkonceniController.getPUkonceniClenstviConverter() {
        return new Converter<PUkonceniClenstvi, String>() {
            public String convert(PUkonceniClenstvi PUkonceniClenstvi) {
                return new StringBuilder().append(PUkonceniClenstvi.getDatumUkonceni()).append(" ").append(PUkonceniClenstvi.getPoznamka()).toString();
            }
        };
    }
    
    @PostConstruct
    void PDuvodyUkonceniController.registerConverters() {
        conversionService.addConverter(getPDuvodyUkonceniConverter());
        conversionService.addConverter(getPUkonceniClenstviConverter());
    }
    
    private String PDuvodyUkonceniController.encodeUrlPathSegment(String pathSegment, HttpServletRequest request) {
        String enc = request.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        }
        catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
