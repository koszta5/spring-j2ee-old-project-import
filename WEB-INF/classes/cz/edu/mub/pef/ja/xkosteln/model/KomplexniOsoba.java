package cz.edu.mub.pef.ja.xkosteln.model;

import java.util.Set;



public class KomplexniOsoba {

	private PAdresy pAdresy;
	private POsoby pOsoby;
	
	public POsoby getpOsoby() {
		return pOsoby;
	}

	public void setpOsoby(POsoby pOsoby) {
		this.pOsoby = pOsoby;
	}
	

	public PAdresy getpAdresy() {
		return pAdresy;
	}
	
	public void setpAdresy(PAdresy pAdresy) {
		this.pAdresy = pAdresy;
	}
	
	public KomplexniOsoba() {
		super();
		this.setpAdresy(new PAdresy());
		this.setpOsoby(new POsoby());

		// TODO Auto-generated constructor stub
		
	}
	 public Set<PClenstvi> getPClenstvis() {
	        return this.pOsoby.getPClenstvis();
	    }
	    
	    public void setPClenstvis(Set<PClenstvi> pClenstvis) {
	        this.pOsoby.setPClenstvis(pClenstvis);
	    }
	    
	    public Set<PDeti> getPDetis() {
	        return this.pOsoby.getPDetis();
	    }
	    
	    public void setPDetis(Set<PDeti> pDetis) {
	        this.pOsoby.setPDetis(pDetis);
	    }
	    
	    public Set<PDeti> getPDetis1() {
	        return this.pOsoby.getPDetis1();
	    }
	    
	    public void setPDetis1(Set<PDeti> pDetis1) {
	        this.pOsoby.setPDetis1(pDetis1);
	    }
	    
	    public Set<PKazen> getPKazens() {
	        return this.pOsoby.getPKazens();
	    }
	    
	    public void setPKazens(Set<PKazen> pKazens) {
	        this.pOsoby.setPKazens(pKazens);
	    }
	    
	    public Set<PKontakty> getPKontakties() {
	        return this.pOsoby.getPKontakties();
	    }
	    
	    public void setPKontakties(Set<PKontakty> pKontakties) {
	        this.pOsoby.setPKontakties(pKontakties);
	    }
	    
	    public Set<PKrty> getPKrties() {
	        return this.pOsoby.getPKrties();
	    }
	    
	    public void setPKrties(Set<PKrty> pKrties) {
	        this.pOsoby.setPKrties(pKrties);
	    }
	    
	    public Set<PMinuleAdresy> getPMinuleAdresies() {
	        return this.pOsoby.getPMinuleAdresies();
	    }
	    
	    public void setPMinuleAdresies(Set<PMinuleAdresy> pMinuleAdresies) {
	        this.pOsoby.setPMinuleAdresies(pMinuleAdresies);
	    }
	    
	    public Set<PPrevody> getPPrevodies() {
	        return this.pOsoby.getPPrevodies();
	    }
	    
	    public void setPPrevodies(Set<PPrevody> pPrevodies) {
	        this.pOsoby.setPPrevodies(pPrevodies);
	    }
	    
	    public Set<PSvatby> getPSvatbies() {
	        return this.pOsoby.getPSvatbies();
	    }
	    
	    public void setPSvatbies(Set<PSvatby> pSvatbies) {
	        this.pOsoby.setPSvatbies(pSvatbies);
	    }
	    
	    public Set<PSvatby> getPSvatbies1() {
	        return this.pOsoby.getPSvatbies1();
	    }
	    
	    public void setPSvatbies1(Set<PSvatby> pSvatbies1) {
	        this.pOsoby.setPSvatbies1(pSvatbies1);
	    }
	    
	    public Set<PUkonceniClenstvi> getPUkonceniClenstvis() {
	        return this.pOsoby.getPUkonceniClenstvis();
	    }
	    
	    public void setPUkonceniClenstvis(Set<PUkonceniClenstvi> pUkonceniClenstvis) {
	        this.pOsoby.setPUkonceniClenstvis(pUkonceniClenstvis);
	    }
	    
	    public PAdresy getPAdresy() {
	        return this.pOsoby.getPAdresy();
	    }
	    
	    public void setPAdresy(PAdresy pAdresy) {
	        this.pOsoby.setPAdresy(pAdresy);
	    }
	    
	    public PStavy getPStavy() {
	        return this.pOsoby.getPStavy();
	    }
	    
	    public void setPStavy(PStavy pStavy) {
	        this.pOsoby.setPStavy(pStavy);
	    }
	    
	    public String getJmeno() {
	        return this.pOsoby.getJmeno();
	    }
	    
	    public void setJmeno(String jmeno) {
	        this.pOsoby.setJmeno(jmeno);
	    }
	    
	    public String getPrijmeni() {
	        return this.pOsoby.getPrijmeni();
	    }
	    
	    public void setPrijmeni(String prijmeni) {
	        this.pOsoby.setPrijmeni(prijmeni);
	    }
	    
	    public String getRodnePrijmeni() {
	        return this.pOsoby.getRodnePrijmeni();
	    }
	    
	    public void setRodnePrijmeni(String rodnePrijmeni) {
	        this.pOsoby.setRodnePrijmeni(rodnePrijmeni);
	    }
	    
	    public String getMestoNarozeni() {
	        return this.pOsoby.getMestoNarozeni();
	    }
	    
	    public void setMestoNarozeni(String mestoNarozeni) {
	        this.pOsoby.setMestoNarozeni(mestoNarozeni);
	    }
	    
	    public Integer getDenNarozeni() {
	        return this.pOsoby.getDenNarozeni();
	    }
	    
	    public void setDenNarozeni(Integer denNarozeni) {
	        this.pOsoby.setDenNarozeni(denNarozeni);
	    }
	    
	    public Integer getMesicNarozeni() {
	        return this.pOsoby.getMesicNarozeni();
	    }
	    
	    public void setMesicNarozeni(Integer mesicNarozeni) {
	        this.pOsoby.setMesicNarozeni(mesicNarozeni);
	    }
	    
	    public Integer getRokNarozeni() {
	        return this.pOsoby.getRokNarozeni();
	    }
	    
	    public void setRokNarozeni(Integer rokNarozeni) {
	        this.pOsoby.setRokNarozeni(rokNarozeni);
	    }
	    
	
	
	public Set<POsoby> getpOsobies() {
	return pAdresy.getPOsobies();
}

public PPscMesta getpPscMesta() {
	return pAdresy.getPPscMesta();
}

public String getUlice() {
	return pAdresy.getUlice();
}

public Integer getCp() {
	return pAdresy.getCp();
}

public String getZeme() {
	return pAdresy.getZeme();
}

	public void setpOsobies(Set<POsoby> pOsobies) {
	this.pAdresy.setPOsobies(pOsobies);
}

public void setpPscMesta(PPscMesta pPscMesta) {
	this.pAdresy.setPPscMesta(pPscMesta);
}

public void setUlice(String ulice) {
	this.pAdresy.setUlice(ulice);
}

public void setCp(Integer cp) {
	this.pAdresy.setCp(cp);
}

public void setZeme(String zeme) {
	this.pAdresy.setZeme(zeme);
}









}