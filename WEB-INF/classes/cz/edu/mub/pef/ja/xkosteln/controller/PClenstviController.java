package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PClenstvi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "pclenstvis", formBackingObject = PClenstvi.class)
@RequestMapping("/pclenstvis")
@Controller
public class PClenstviController {
}
