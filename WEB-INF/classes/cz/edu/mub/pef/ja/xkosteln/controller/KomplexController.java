package cz.edu.mub.pef.ja.xkosteln.controller;


import java.io.UnsupportedEncodingException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;


import cz.edu.mub.pef.ja.xkosteln.model.KomplexniOsoba;
import cz.edu.mub.pef.ja.xkosteln.model.PAdresy;
import cz.edu.mub.pef.ja.xkosteln.model.POsoby;
import cz.edu.mub.pef.ja.xkosteln.model.PPscMesta;
import cz.edu.mub.pef.ja.xkosteln.model.PStavy;


@RequestMapping("/komplex/**")
@Controller
public class KomplexController{
	


	
    @RequestMapping(method = RequestMethod.POST)
    public String create(KomplexniOsoba ko, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("KomplexniOsoba", ko);
            return "komplex/create";
        }
        PAdresy pa=ko.getpAdresy();
        POsoby po=ko.getpOsoby();
        pa.persist();
        po.setPAdresy(pa);
        	po.persist();	
       
        return "redirect:/posobys/" + encodeUrlPathSegment(po.getIdOsoby().toString(), request);
    }
    
    
	@RequestMapping(params = "form", method = RequestMethod.GET)
    public String createForm(Model model){
		model.addAttribute("KomplexniOsoba", new KomplexniOsoba());
		return "komplex/create";
	}    

	 @ModelAttribute("padresys")
	    public Collection<PAdresy> populatePAdresys() {
	        return PAdresy.findAllPAdresys();
	    }
	 
	 @ModelAttribute("pstavys")
	 	public Collection<PStavy> populatePStavies(){
		 return PStavy.findAllPStavys();
	 }
	 @ModelAttribute("ppscmestas")
	 	public Collection<PPscMesta> populatePPscMestas(){
		 return PPscMesta.findAllPPscMestas();
	 }

	

	
	
	 private String encodeUrlPathSegment(String pathSegment, HttpServletRequest request) {
	        String enc = request.getCharacterEncoding();
	        if (enc == null) {
	            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
	        }
	        try {
	            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
	        }
	        catch (UnsupportedEncodingException uee) {}
	        return pathSegment;
	    }
	
	
}

