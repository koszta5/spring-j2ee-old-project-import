package cz.edu.mub.pef.ja.xkosteln.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.edu.mub.pef.ja.xkosteln.ireport.FinalYearReport;
import cz.edu.mub.pef.ja.xkosteln.ireport.NarozeninyReport;




@RequestMapping("/ireport/**")
@Controller
public class JasperReportController {
	
	 
    @RequestMapping(value="/ireport/narozeniny", method=RequestMethod.GET)
    public String createreport(Model model, HttpServletRequest rq){
    	NarozeninyReport nar=new NarozeninyReport();
    	model.addAttribute("nar", nar);
    	String ProjectDeployPath=rq.getSession().getServletContext().getRealPath("");
    	String jrxmlPath=ProjectDeployPath + "/WEB-INF/Jasper/report1.jrxml";
    	
    	nar.createReport(jrxmlPath);
    	return "ireport/narozeniny";
    }
    @RequestMapping (value="/ireport/final", method=RequestMethod.GET)
    public String createFinalReport(Model model,HttpServletRequest rq){
    	FinalYearReport fin=new FinalYearReport();
    	model.addAttribute("fin", fin);
    	String ProjectDeployPath=rq.getSession().getServletContext().getRealPath("");
    	String jrxmlPath=ProjectDeployPath + "/WEB-INF/Jasper/report3.jrxml";
    	fin.createReport(jrxmlPath);
    	return "ireport/final";
    }
  
	
}
