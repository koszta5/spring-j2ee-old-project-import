// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package cz.edu.mub.pef.ja.xkosteln.model;

import cz.edu.mub.pef.ja.xkosteln.model.PKontakty;
import java.lang.Integer;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PKontakty_Roo_Entity {
    
    declare @type: PKontakty: @Entity;
    
    declare @type: PKontakty: @Table(name = "p_kontakty", catalog = "kosta");
    
    @PersistenceContext
    transient EntityManager PKontakty.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_kontakty")
    private Integer PKontakty.idKontakty;
    
    public Integer PKontakty.getIdKontakty() {
        return this.idKontakty;
    }
    
    public void PKontakty.setIdKontakty(Integer id) {
        this.idKontakty = id;
    }
    
    @Transactional
    public void PKontakty.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void PKontakty.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            PKontakty attached = this.entityManager.find(this.getClass(), this.idKontakty);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void PKontakty.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public PKontakty PKontakty.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        PKontakty merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager PKontakty.entityManager() {
        EntityManager em = new PKontakty().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long PKontakty.countPKontaktys() {
        return entityManager().createQuery("select count(o) from PKontakty o", Long.class).getSingleResult();
    }
    
    public static List<PKontakty> PKontakty.findAllPKontaktys() {
        return entityManager().createQuery("select o from PKontakty o", PKontakty.class).getResultList();
    }
    
    public static PKontakty PKontakty.findPKontakty(Integer id) {
        if (id == null) return null;
        return entityManager().find(PKontakty.class, id);
    }
    
    public static List<PKontakty> PKontakty.findPKontaktyEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("select o from PKontakty o", PKontakty.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
