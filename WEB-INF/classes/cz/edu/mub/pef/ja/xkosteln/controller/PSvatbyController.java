package cz.edu.mub.pef.ja.xkosteln.controller;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import cz.edu.mub.pef.ja.xkosteln.model.PSvatby;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "psvatbys", formBackingObject = PSvatby.class)
@RequestMapping("/psvatbys")
@Controller
public class PSvatbyController {
}
